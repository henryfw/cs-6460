var app = new Vue({
    el: '#app',

    methods: {
        getTextAreaValue: function() {
            return $("#raw-text").val();
        },
        loadText: function() {
            if (this.viewer) this.viewer.destroy();
            this.viewer = new Viewer("#text", this.getOptions());
            this.viewer.loadText(  this.getTextAreaValue() );
            this.showForm = false;
        },
        selectSample: function(e) {
            let file = e.target.value;
            if (file) {
                if (this.viewer) this.viewer.destroy();
                this.viewer = new Viewer("#text", this.getOptions());
                this.viewer.loadData(file);
                this.showForm = false;
            }
        },
        updateOptions: function (e) {
            window.localStorage.setItem("options", JSON.stringify(this.getOptions()));
            this.viewer.updateOptions(this.getOptions())
        },
        getOptions: function() {
            return {
                letterSpacing: this.optionLetterSpacing,
                markBeginningOfSentence: this.optionMarkBeginningOfSentence && this.optionHighlightLevel == 'none',
                highlightLevel: this.optionHighlightLevel
            }
        }
    },
    beforeMount: function() {
        let data = window.localStorage.getItem("options");
        if (data) {
            let opt = JSON.parse(data);
            this.optionLetterSpacing = opt.letterSpacing;
            this.optionMarkBeginningOfSentence = opt.markBeginningOfSentence;
            this.optionHighlightLevel = opt.highlightLevel;
        }
    },
    mounted: function () {
        if ( this.getTextAreaValue() ) {
            this.loadText();
        }
    },
    data: {
        viewer: null,
        showForm: true,
        samples: {
            "dream" : "I Have a Dream Speech (Excerpt)",
            "navy" : "The British Navy in Battle (Excerpt)",
            "common" : "Common Sense (Excerpt)"
        },
        optionLetterSpacing: 1,
        optionMarkBeginningOfSentence: true,
        optionHighlightLevel: "none"
    }
});





// var fontLoader = new FontLoader(["Libre Baskerville"], {
//     "complete": function(error) {
//         if (error !== null) {
//             // Reached the timeout but not all fonts were loaded
//             console.log(error.message);
//             console.log(error.notLoadedFonts);
//         } else {
//             // All fonts were loaded
//             console.log("all fonts were loaded");
//         }
//     }
// }, 3000);
// fontLoader.loadFonts();
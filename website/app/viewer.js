(function($, nlp) {

    class Viewer {

        constructor(div, opt) {
            this.div = div;
            this.nlp = null;
            this.phrases = [];
            this.pages = [];
            this.fakeDomContainer = $(`<div/>`);
            this.rawText = "";

            this.options = $.extend({
                letterSpacing: 1,
                markBeginningOfSentence: true,
                highlightLevel: "none", // values can be "none", "phrase", "sentence"
            }, opt);

            this.fakeDomContainer.appendTo(document.body);
        }

        updateOptions(opt) {
            this.options = $.extend({}, this.options, opt);
            this.phrases = [];
            this.pages = [];
            this.loadText(this.rawText);
        }

        destroy() {
            this.nlp = null;
            this.phrases = [];
            this.pages = [];
            this.fakeDomContainer.remove();
            this.rawText = "";
        }

        loadData(file) {
            $.get(`data/${file}.txt`).then( (r) => {
                this.loadText(r);
            });
        }

        loadText(r) {
            this.rawText = r;
            this.nlp = nlp(r);

            this.makePhrases();
            this.makePages();
            this.showPages();
        }


        binarySearchForHeight(start, end, targetHeight) {
            let fakeDom = $(`<div class="page fake-page ${this.options.markBeginningOfSentence ? "marked-sentence" : ""}"
                style="letter-spacing: ${this.options.letterSpacing}px" />`);
            $(fakeDom).appendTo(this.fakeDomContainer);

            let currentEnd = end;
            let prevCurrentEnd = end;

            while(true) {
                fakeDom.html( this.phrases.slice(start, currentEnd + 1) );
                let currentHeight = fakeDom.height();
                if (currentHeight > targetHeight) {
                    prevCurrentEnd = currentEnd;
                    currentEnd = Math.floor( (currentEnd - start) / 2 ) + start;
                }
                else { // lower bound of page at currentEnd and upper bound is prevCurrentEnd

                    for (let i = currentEnd; i < prevCurrentEnd; i ++) {  // linear search between last 2 bounds
                        fakeDom.html( this.phrases.slice(start, i + 1) );
                        currentHeight = fakeDom.height();
                        if (currentHeight > targetHeight) {
                            currentEnd = i - 1;
                            console.log(`Found page at ${start}, ${currentEnd}, ${currentHeight} `);
                            break;
                        }
                        currentEnd = i;

                    }
                    break;
                }
            }
            fakeDom.remove();

            return currentEnd;
        }

        showPages() {
            $(this.div).empty().append(this.pages);
        }

        makePages() {
            let viewPortHeight = $( window ).height() - 80;
            let currentStart = 0;
            let total = this.phrases.length;
            let pageNumber = 1;


            while (true) {
                let currentEnd = this.binarySearchForHeight(currentStart, total - 1, viewPortHeight);
                let lastPage = currentEnd == total - 1;

                this.pages.push($(`<a name="${pageNumber}"></a>`));
                let page = $(`<div class="page ${this.options.markBeginningOfSentence ? "marked-sentence" : ""}" 
                    style="height: ${viewPortHeight + 40}px; letter-spacing: ${this.options.letterSpacing}px" />`);

                page.append(this.phrases.slice(currentStart, currentEnd + 1));

                this.pages.push(page);

                let pageFooter = `<div class="page-footer">`;
                if (pageNumber > 1) pageFooter += `<a href='#${pageNumber-1}'>&lt;&lt;</a> &nbsp; `;
                pageFooter += `Page ${pageNumber}`;
                if (!lastPage) pageFooter += ` &nbsp;  <a href='#${pageNumber+1}'>&gt;&gt;</a>`;

                pageFooter += `</div>`;
                this.pages.push(pageFooter);

                currentStart = currentEnd + 1;
                pageNumber++;

                if (lastPage) break;
            }
        }

        makePhrases() {
            let container = document.createElement("div");

            /*
             rules for phases:
             end of sentence ends the phrase
             no: Copula starts new
             ClauseEnd ends the phrase
             no: if Preposition starts a long segment, then new phrase.
             no: if Conjunction starts a long segment, then new phrase
             afterWord == ";" is end phrase
             QuestionWord starts a new phrase if current phrase is long
             Determiner "that" if starts a long segment
             double dash is a new phrase
            */

            let phrases = []; // array of "<span>this is a phrase</span>"
            let phraseCount = 0;
            let pendingPhrase = []; // array of words to go into one phrase
            let addPhrase = (word) => {
                pendingPhrase.push(word);
            };
            let endPhrase = (force) => {
                if (!force && pendingPhrase.length < 3) return;

                let className = "";
                let line = `<span class="phrase">${pendingPhrase.join("")}</span>`;

                if (this.options.highlightLevel != "none" && phraseCount % 2 == 0) {
                    let innerLine = pendingPhrase.join("");
                    let prefix = "";
                    let suffix = "";
                    for (let i = 0; i < innerLine.length; i ++) {
                        if (innerLine.charAt(i) == " ") prefix += " ";
                        else break;
                    }
                    for (let i = innerLine.length -1 ; i >= 0; i --) {
                        if (innerLine.charAt(i) == " ") suffix += " ";
                        else break;
                    }
                    line = `<span class="phrase">${prefix}<span class="alt-phrase">${innerLine.substring(prefix.length, innerLine.length - suffix.length)}</span>${suffix}</span>`;
                }

                phrases.push($(line)[0]);
                pendingPhrase = [];
                phraseCount ++ ;
            };
            let endParagraph = () => {
                phrases.push("<br/>");
                phrases.push("<br/>");
            };

            this.nlp.sentences().data().forEach((sentence) => {

                if (this.options.highlightLevel != "phrase") {
                    addPhrase(sentence.text + (sentence.text.charAt(sentence.text.length-1) != " " ? " " : ""));
                    endPhrase(true);
                    if (sentence.text.indexOf("\n") != -1) {
                        endParagraph();
                    }
                }
                else {

                    let terms = nlp(sentence.text).terms().data();

                    terms.forEach((term, termIndex) => {

                        let tags = term.tags;
                        let spaceAfter = term.spaceAfter;
                        let spaceBefore = term.spaceBefore;
                        // if (spaceBefore == " ") spaceBefore = "&nbsp; "
                        // if (spaceAfter == " ") spaceAfter = "&nbsp; "
                        // let text = `${spaceBefore}<span title="${tags.join(",")}">${term.text}</span>${spaceAfter}`;
                        let text = `${spaceBefore}${term.text}${spaceAfter}`;


                        if (termIndex == terms.length - 1) {
                            addPhrase(text);
                            endPhrase(true);
                            if (spaceAfter.indexOf("\n") != -1) {
                                endParagraph();
                            }
                        }
                        // else if (tag == "Copula") {
                        //     endPhrase();
                        //     addPhrase(text);
                        // }
                        else if (tags.indexOf("Preposition") != -1) {
                            endPhrase();
                            addPhrase(text);
                        }
                        else if (tags.indexOf("Conjunction") != -1) {
                            endPhrase();
                            addPhrase(text);
                        }
                        else if ( tags.indexOf("ClauseEnd") != -1) {
                            addPhrase(text);
                            endPhrase();
                        }
                        else {
                            addPhrase(text);
                        }
                    })
                }


                // let text = this.nlp.terms().data().map(a => {
                //     let spaceAfter = a.spaceAfter;
                //     if (spaceAfter && spaceAfter.indexOf("\n") != -1) spaceAfter = "<p>";
                //     let bestTag = `<span class='light'>${a.bestTag}</span>`;
                //     // if (bestTag != "(Preposition)") bestTag = "";
                //     return `${a.text} ${bestTag} ${spaceAfter}`
                // }).join("");

                this.phrases = phrases;
            });
        }

    }

    window.Viewer = Viewer

}($, nlp));
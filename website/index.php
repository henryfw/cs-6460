<?php

$text = "";

if (isset($_GET['url'])) {
    $url=$_GET['url'];
    $agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_USERAGENT, $agent);
    curl_setopt($ch, CURLOPT_URL,$url);
    $raw=curl_exec($ch);

    $article_start = strpos($raw, "<article");
    $article_end = strpos($raw, "</article");
    if ($article_start !== false){
        $article = substr($raw, $article_start, $article_end - $article_start);

        $text = strip_tags($article, '<p><h1><h2><h3><h4><h5><h6>');
        $text = preg_replace('/(<p)/', "\n\n<p", $text);
        $text = preg_replace('/(<h)/', "\n\n<h", $text);
        $text = trim(strip_tags($text));
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Viewer</title>
    <script src="libs/compromise/compromise.min.js"></script>
    <script src="libs/jquery/jquery-3.1.1.js"></script>
    <script src="libs/font-loader/font-loader.js"></script>
    <script src="libs/vue/vue.min.js"></script>
    <script src="app/viewer.js"></script>

    <link href="css/foundation.min.css" rel="stylesheet" />
    <link href="css/viewer.css" rel="stylesheet" />

</head>
<body>

<div id="app">
    <form class="input-form" id="input" v-show="showForm" >
        <div class="row">
            <div class="columns small-12">
                <textarea style="width:100%" rows="30" id="raw-text"><?php echo $text ?></textarea>
            </div>
        </div>
        <div class="row">
            <div class="columns small-6">
                <input type="button" style="width: 100%" class="button  " value="Load Text Into Columns" v-on:click="loadText()" />
            </div>
            <div class="columns small-6">
                <select class="inline" v-on:change="selectSample" >
                    <option value=""> - Or Select Sample Text - </option>
                    <option v-for="(v,k) in samples" v-bind:value="k">{{v}}</option>
                </select>
            </div>
        </div>
    </form>

    <div  class="row">
        <div id="text" class="viewer columns small-10" v-show="!showForm"></div>
        <div class="columns small-2 option-container" v-show="!showForm" >


            <a  v-on:click="showForm = true">Reset</a>

            <hr>

            <label>
                Letter Spacing
                <input type="number" v-model="optionLetterSpacing">
            </label>


            <label>Highlight
                <select v-model="optionHighlightLevel">
                    <option value="none">None</option>
                    <option value="sentence">Alternate Sentence</option>
                    <option value="phrase">Alternate Phrase</option>
                </select>
            </label>

            <label v-show="optionHighlightLevel == 'none'">
                Sentence Maker <br>
                <input type="checkbox" v-model="optionMarkBeginningOfSentence" >
            </label>

            <input type="button" class="button small" value="Update" style="width: 100%" v-on:click="updateOptions">
        </div>
    </div>
</div>

<script src="app/app.js"></script>

</body>
</html>
